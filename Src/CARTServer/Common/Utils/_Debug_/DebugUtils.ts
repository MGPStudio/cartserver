'use strict';

import { LogType } from "./LogType";
import { DebugContextType } from "./DebugContextType";
import { ICARTCore } from "../../../ICARTCore";

export class DebugUtils {
	constructor(_contextType: DebugContextType, _context: ICARTCore) {
		this.contextType_ = _contextType;
		this.context_ = _context;
	}

	public Assert(_stuff: any, _context: object) {
		if (!_stuff) this.Log(LogType.ERR, true, "Assertion Failed: " + _stuff.toString() + " \n\t\tin " + _context.toString());
	}

	public Log(_logType: LogType, _withTimestamp: boolean, _logMessage: string) {
		if (_logMessage.length === 0) {
			this.InternalLog(LogType.WARN, "Error formatting the log message (empty message string).", "");
			return;
		}

		var dateString: string = "";
		if (_withTimestamp) dateString = "[" + new Date().toISOString() + "]";

		this.InternalLog(_logType, _logMessage, dateString);
	}

	private InternalLog(_logType: LogType, _logMessage: string, _logTimeDate: string) {
		var contextString_: string = "";
		switch (this.contextType_) {
			case DebugContextType.CART_FRONTEND: {
				contextString_ = "CARTFrontend";
				break;
			}
			case DebugContextType.CART_BACKEND: {
				contextString_ = "CARTBackend";
				break;
			}
		}

		switch (_logType) {
			case LogType.INFO: {
				console.log(_logTimeDate + "[" + contextString_ + "][INFO] " + _logMessage);
				break;
			}
			case LogType.WARN: {
				console.log(_logTimeDate + "[" + contextString_ + "][WARN] " + _logMessage);
				break;
			}
			case LogType.ERR: {
				console.log(_logTimeDate + "[" + contextString_ + "][ERROR] " + _logMessage);
				this.context_.Shutdown(-1);
				break;
			}
		}
		
	}

	private contextType_: DebugContextType;
	private context_: ICARTCore;
}