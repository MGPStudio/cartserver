var overlays;
var overlaysCount;

function InitCARTServerClient() {
	CreateNavMenuTooltips();
	InitOverlays();
	CloseManageContainer();
}

function InitOverlays() {
	overlays = new Array();
	overlaysCount = 0;

	overlays[overlaysCount] = "#NavMenu-HeaderButton-MGPConnectionStatus";
	InitMGPConnectionStatusOverlay();
	overlaysCount++;
	overlays[overlaysCount] = "#NavMenu-HeaderButton-MGPMenu";
	InitMGPMenuOverlay();
	overlaysCount++;
	overlays[overlaysCount] = "#NavMenu-HeaderButton-AccountInfo";
	InitAccountInfoOverlay();
	overlaysCount++;
}

function InitMGPConnectionStatusOverlay() {
	$("#NavMenu-HeaderButton-MGPConnectionStatus").popover({
		template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header text-white bg-dark"></h3><div class="popover-body text-white bg-dark"></div></div>',
		title: "Connection Status",
		placement: 'bottom',
		html: true,
		content: 
		`<div id="Overlay-MGPConnectionStatus-Container" class="container">
			<div class="row-fluid">
				<div class="text-white bg-dark d-inline-flex flex-row Overlay-MGPConnectionStatus-Label">Connection: </div>
				<div id="Overlay-MGPConnectionStatus-NetworkValue" class="d-inline-flex flex-row Overlay-MGPConnectionStatus-Value">alive</div>
			<div>
			<div class="row-fluid">
				<div class="text-white bg-dark d-inline-flex flex-row Overlay-MGPConnectionStatus-Label">Last update: </div>
				<div id="Overlay-MGPConnectionStatus-LastUpdatedValue" class="d-inline-flex flex-row Overlay-MGPConnectionStatus-Value">16-05-2019 00:30:01</div>
			<div>
		</div>`
	});
}

function InitMGPMenuOverlay() {
	$("#NavMenu-HeaderButton-MGPMenu").popover({
		template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header text-white bg-dark"></h3><div class="popover-body text-white bg-dark"></div></div>',
		placement: 'bottom',
		html: true,
		content: 
		`<div id="Overlay-MGPMenu-Container" class="container">
			<div class="row-fluid mt-5 mb-5">
				<div class="text-light bg-dark d-inline-flex flex-row ml-5 mr-5 Overlay-MGPMenuStatus-Label">Coming Soon</div>
			<div>
		</div>`
	});
}

function InitAccountInfoOverlay() {
	$("#NavMenu-HeaderButton-AccountInfo").popover({
		template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header text-white bg-dark"></h3><div class="popover-body text-white bg-dark"></div></div>',
		placement: 'bottom',
		html: true,
		content: 
		`<div id="Overlay-AccountInfo-Container" class="container">
			<div class="row-fluid mt-5 mb-5">
				<div class="text-light bg-dark d-inline-flex flex-row ml-5 mr-5 Overlay-AccountInfoStatus-Label">Coming Soon</div>
			<div>
		</div>`
	});
}

function CreateNavMenuTooltips() {
	// Header menu
	// $("#NavMenu-HeaderButton-MGPConnectionStatus").popover({
	// 	placement: 'bottom',
	// 	html: false,
	// 	content: "Connection Status"
	// });
	// $("#NavMenu-HeaderButton-MGPMenu").popover({
	// 	placement: 'bottom',
	// 	html: false,
	// 	content: "MGP Projects"
	// });
	// $("#NavMenu-HeaderButton-AccountInfo").popover({
	// 	placement: 'bottom',
	// 	html: false,
	// 	content: "Account Info"
	// });
	$("#NavMenu-HeaderButton-SignOut").popover({
		placement: 'bottom',
		html: false,
		content: "Sign Out"
	});


	// Side menu
	$("#NavMenu-Side-SectionButton-Bugs").popover({
		placement: 'right',
		html: false,
		content: "Bugs"
	});
	$("#NavMenu-Side-SectionButton-Events").popover({
		placement: 'right',
		html: false,
		content: "Events"
	});
	$("#NavMenu-Side-SectionButton-Apps").popover({
		placement: 'right',
		html: false,
		content: "Apps"
	});
	$("#NavMenu-Side-SectionButton-Settings").popover({
		placement: 'right',
		html: false,
		content: "Settings"
	});
}

function ShowTooltip(elementId) {
	$(elementId).popover('enable');
	$(elementId).popover('show');
}

function HideTooltip(elementId) {
	$(elementId).popover('hide');
	$(elementId).popover('disable');
}

function OpenManageContainer() {
	document.getElementById("MainActionButtons-ManageContainer").setAttribute("style", "display: inline-flex !important");
	document.getElementById("MainActionButtons-Manage").setAttribute("style", "display: none !important");

	var tableElement = document.getElementById("MainDataTable");
	var tableRowHTML = tableElement.getElementsByClassName("TableColumnDescriptorRow")[0].innerHTML;
	tableElement.getElementsByClassName("TableColumnDescriptorRow")[0].innerHTML = `
		<div class="col TableColumn TableSelectAllRowsColumn">
			<input type="checkbox" class="form-check-input TableSelectAllRowsCheck" onclick="ToggleSelectAllRows()">
		</div>
	` + tableRowHTML;

	var tableDataRows = tableElement.getElementsByClassName("TableDataRow");
	Array.from(tableDataRows).forEach(row => {
		tableRowHTML = row.innerHTML;

		row.innerHTML = `
		<div class="col TableColumn TableSelectAllRowsColumn">
			<input type="checkbox" class="form-check-input TableSelectAllRowsCheck">
		</div>
	` + tableRowHTML;
	});
}

function CloseManageContainer() {
	document.getElementById("MainActionButtons-Manage").setAttribute("style", "display: inline-flex !important");
	document.getElementById("MainActionButtons-ManageContainer").setAttribute("style", "display: none !important");

	var selectRowColumn = document.getElementById("MainDataTable").getElementsByClassName("TableSelectAllRowsColumn")[0];
	GetTableColumnDescRow().removeChild(selectRowColumn);
	var tableDataRows = GetTableDataRows();
	Array.from(tableDataRows).forEach(row => {
		row.removeChild(row.getElementsByClassName("TableSelectAllRowsColumn")[0]);
	});
}

function GetTableDataRows() {
	return document.getElementById("MainDataTable").getElementsByClassName("TableDataRow");
}

function GetTableColumnDescRow() {
	return document.getElementById("MainDataTable").getElementsByClassName("TableColumnDescriptorRow")[0];
}

function ToggleSelectAllRows() {
	if (document.getElementsByClassName("TableSelectAllRowsCheck")[0].checked) {
		GetTableDataRows().forEach(row => {
			row.getElementsByClassName("TableSelectRowCheck")[0].checked = true;
		});
	}
}