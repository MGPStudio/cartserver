'use strict';

import { ICARTCore } from "../ICARTCore";
import { CARTIPCProvider } from "../Common/Utils/CARTIPCProvider";
import { DebugUtils } from "../Common/Utils/_Debug_/DebugUtils";
import { DebugContextType } from "../Common/Utils/_Debug_/DebugContextType";
import { SessionManager } from "./SessionManager";
import { CIPCProviderType } from "../Common/Utils/CIPCProviderType";
import { LogType } from "../Common/Utils/_Debug_/LogType";

export class CARTBackendCore implements ICARTCore {
	constructor() {
		this._debugUtils_ = new DebugUtils(DebugContextType.CART_BACKEND, this);
		this.sessionManager_ = new SessionManager(this, 5);
		
		var ipcSecret: string = (process.env.CARTSERVER_BACKEND_IPC_SECRET || "");
		if (ipcSecret.length === 0) this._debugUtils_.Log(LogType.ERR, true, "CARTBackendCore Init Error: IPC secret not defined.");
		this.ipcProvider_ = new CARTIPCProvider(CIPCProviderType.CART_BACKEND, "", 0, ipcSecret);
	}

	public GetIPCProvider(): CARTIPCProvider {
		throw new Error("Method not implemented.");
	}

	Shutdown(_errCode: number): void {
		// Nothing to shutdown from here, implement later
		process.exit(_errCode);
	}
	
	private ipcProvider_: CARTIPCProvider;
	private sessionManager_: SessionManager;
	public _debugUtils_: DebugUtils;
}