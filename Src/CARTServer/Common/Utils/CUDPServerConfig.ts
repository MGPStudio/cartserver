'use strict';

const AFK_TIMEOUT_DEFAULT = 60000;

export class CUDPServerConfig {
	constructor(_port: number, _maxActiveConn: number, _afkTimeout: number) {
		// Assert(_port > 0);
		// Assert(_maxActiveConn > 0);
		// Assert(_afkTimeout >= 0);

		this.port_ = _port;
		this.maxActiveConn_ = _maxActiveConn;
		if (_afkTimeout === 0) this.afkTimeout_ = AFK_TIMEOUT_DEFAULT;
		else this.afkTimeout_ = _afkTimeout;
	}

	public port_: number;
	public maxActiveConn_: number;
	public afkTimeout_: number;
}