export interface ICARTDBManagerCallbackProvider {
	OnConnectCallback(_success: boolean): void;
	OnGetSingleStorageDataCallback(_data: any): void;
}