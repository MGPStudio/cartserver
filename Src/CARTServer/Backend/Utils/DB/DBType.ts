/*
 * Copyright (С) MGP Studio 2019, All Rights Reserved.
 * This document is a property of MGP Studio and its distribution is prohibited.
 * INTERNAL USE ONLY
 * 
 * Project: CARTServer (MGPStudio-Web-Internal-CARTServer)
 * Type: Web
 * 
 * File name: DBType.ts
 * Location: /CARTServer/Utils/DB/
 */

'use strict';

export enum DBType {
	UNDEFINED = 0,
	MYSQL = 1,
	MONGODB = 2,
	FIREBASE = 3 // Not imemented yet
}