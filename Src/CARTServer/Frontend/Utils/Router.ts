'use strict';

import { ICARTCore } from "../../ICARTCore";
import { CHTTP2ClientRequest } from "../../Common/Utils/CHTTP2ClientRequest";
import { CHTTP2ServerResponse } from "../../Common/Utils/CHTTP2ServerResponse";
import { CHTTPStatusCode } from "../../Common/Utils/CHTTPStatusCode";

export class Router {
	constructor(_context: ICARTCore, _routeList: object, _dirConfig: object) {
		this.context_ = _context;
		this.routeList_ = _routeList;
		this.dirConfig_ = _dirConfig;
	}

	public GetRoutedObject(_request: CHTTP2ClientRequest): CHTTP2ServerResponse {
		return new CHTTP2ServerResponse(CHTTPStatusCode.ERR_NOT_FOUND, [], "", "Not found.", "", []);
	}

	public context_: ICARTCore;
	public routeList_: object;
	public dirConfig_: object;
}