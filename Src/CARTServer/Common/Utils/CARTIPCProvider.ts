'use strict';

import { CUDPClient } from "./CUDPClient";
import { CUDPServer } from "./CUDPServer";
import { CIPCProviderType } from "./CIPCProviderType";

export class CARTIPCProvider {
	constructor(_providerType: CIPCProviderType, _ipcServerHost: string, _ipcServerPort: number, _connSecret: string) {
		
	}

	private reciever_?: CUDPServer;
	private sender_?: CUDPClient;
}