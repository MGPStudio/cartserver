'use strict';

// TEST ONLY
import { MongoDBManager } from "./Utils/DB/MongoDBManager";
import { DBConfig } from "./Utils/DB/DBConfig";
import { DBManagerCallbackProvider } from "./Utils/DB/DBManagerCallbackProvider";
// END TEST ONLY

export class CARTCore implements DBManagerCallbackProvider {
	private test2: MongoDBManager;

	constructor() {
		var test = new DBConfig();
		test.dbHost_ = "mongodb://192.168.0.101:28090";
		test.dbName_ = "mgpstudio_cartserver";
		test.dbType_ = 2;
		test.dbUser_ = "mmserver-admin";
		test.password_ = "aPDwDPjf90QYCx45eXUl";
		this.test2 = new MongoDBManager(test);
		this.test2.Connect(this);
	}
	
	public OnConnectCallback(_success: boolean): void {
		if (_success) {
			console.log(this.test2.GetConnectionState());
			var aggregator: object[] = [
				{"$sort":{"bug_id": 1}},
				{
					"$project": 
					{
						"_id": 0,
						"bug_id": 1,
						"creation_timestamp": { "$subtract": [ "$creation_timestamp", 0 ] },
						"author": 1,
						"author_account_id": 1,
						"summary": 1,
						"description": 1,
						"state": 1,
						"priority": 1
					}
				}
			];
			this.test2.GetCollectionDocuments("BugReportInfo", aggregator, this);
			
		}
	}

	public OnGetCollectionDocumentsCallback(_docs: any): void {
		console.log(_docs);
	}
}