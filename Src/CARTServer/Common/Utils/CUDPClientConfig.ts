'use strict';

export class CUDPClientConfig {
	constructor(_enableHeartbeat: boolean) {
		this.enableHeartbeat_ = _enableHeartbeat;
	}

	public enableHeartbeat_: boolean;
}