'use strict';

import { WebAPIManager } from "../WebAPIManager";
import { CARTBackendCore } from "../../CARTBackendCore";

export class DBGetCARTEvents {
	constructor(_apiManager: WebAPIManager, _name: string, _isEnabled: boolean) {
		this.apiManager_ = _apiManager;
		this.isEnabled_ = _isEnabled;

		(this.apiManager_.GetContext() as CARTBackendCore)._debugUtils_.Assert(_name.length > 0, this);

		this.name_ = _name;
	}

	public Call(_params: object[]): string {
		return "";
	}

	private name_: string;
	private isEnabled_: boolean;
	private apiManager_: WebAPIManager;
}