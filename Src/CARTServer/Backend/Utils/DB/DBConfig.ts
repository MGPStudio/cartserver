/*
 * Copyright (С) MGP Studio 2019, All Rights Reserved.
 * This document is a property of MGP Studio and its distribution is prohibited.
 * INTERNAL USE ONLY
 * 
 * Project: CARTServer (MGPStudio-Web-Internal-CARTServer)
 * Type: Web
 * 
 * File name: DBConfig.ts
 * Location: /CARTServer/Utils/DB/
 */

'use strict';


import { DBType } from "./DBType";


export class DBConfig {
	constructor() {
		this.dbType_ = DBType.UNDEFINED;
		this.dbHost_ = "";
		this.dbName_ = "";
		this.dbUser_ = "";
		this.password_ = "";
	}
	
	public dbType_: DBType;
	public dbHost_: string;
	public dbUser_: string;
	public password_: string;
	public dbName_: string;
}