'use strict';

import { CHTTP2ServerConfig } from "./CHTTP2ServerConfig";
import { Http2ServerRequest, Http2ServerResponse, IncomingHttpHeaders } from "http2";
import { ICARTCore } from "../../ICARTCore";
import { CARTFrontendCore } from "../../Frontend/CARTFrontendCore";
import { LogType } from "./_Debug_/LogType";
import { CHTTP2ClientRequest } from "./CHTTP2ClientRequest";
import { CHTTP2ServerResponse } from "./CHTTP2ServerResponse";

const http2 = require('http2');
const fs = require('fs');
const {
	HTTP2_HEADER_METHOD,
	HTTP2_HEADER_PATH,
	HTTP2_HEADER_STATUS,
	HTTP2_HEADER_CONTENT_TYPE
  } = http2.constants;

export class CHTTP2Server {
	constructor(_context: ICARTCore, _config: CHTTP2ServerConfig) {
		this.config_ = _config;
		this.context_ = _context;
	}

	public Init() {
		this.server_ = http2.createSecureServer({
			cert: fs.readFileSync(this.config_.sslCertPath_, "utf-8"),
			key: fs.readFileSync(this.config_.sslKeyPath_, "utf-8"),
			ca: fs.readFileSync(this.config_.sslCACertPath_, "utf-8"),
			allowHTTP1: true,
			maxHeaderListPairs: 25,
			peerMaxConcurrentStreams: 10,
			origins: this.config_.allowedOrigins_
		});
		var this_ = this;
		this.server_.on("request", (_request: Http2ServerRequest, _response: Http2ServerResponse) => {
			this_.ClientRequestHandler(_request, _response);
		});
		this.server_.on("error", (_err: Error) => {
			(this_.context_ as CARTFrontendCore)._debugUtils_.Log(LogType.WARN, true, "CHTTP2Server error: " + _err.message);
		});
		this.server_.listen(this.config_.port_);
		(this_.context_ as CARTFrontendCore)._debugUtils_.Log(LogType.INFO, true, "Server started on port " + this.config_.port_);
	}

	private ClientRequestHandler(_request: Http2ServerRequest, _response: Http2ServerResponse): void {
		
		var authToken: string = (_request.headers["authorization"] || "");
		if (authToken.length != 0) authToken = authToken.split("Bearer ")[1];
		var cookies: string[] = (_request.headers["set-cookie"] || [ "" ]);
		var host: string = (_request.headers[":authority"] || _request.headers["host"] || "");
		var port = 0;
		if (host.length != 0) {
			port = parseInt(host.split(":")[1]);
			host = host.split(":")[0];
		}
		var ip: string = (_request.socket.remoteAddress || "");
		var path: string = (_request.headers[":path"] || _request.url);
		var userAgent: string = (_request.headers["user-agent"] || "");
		var headers: object = _request.headers;


		/* TODO: TEST ONLY, REMOVE BEFORE PROD */
		// _response.setHeader("X-MGP-TestHeader", "verified");
		// _response.writeHead(200, { "Content-Type": "text/plain" });
		// _response.end("HTTP/2 test\nHost: " + host + "\nPort: " + port + "\nUser Agent: " + _request.headers["user-agent"] + "\nPath: " + path + "\nCookies: " + cookies + "\nAuth secret: " + authToken + "\nClient IP: " + ip);
		/* ----------------------------- */


		var request: CHTTP2ClientRequest = new CHTTP2ClientRequest(host, port, path, ip, userAgent, cookies, authToken, headers);

		var response: CHTTP2ServerResponse = (this.context_ as CARTFrontendCore).GetRouter().GetRoutedObject(request);
	}

	public Shutdown(): void {
		if (this.server_ != null) this.server_.close();
	}

	private config_: CHTTP2ServerConfig;
	private server_: any;
	private context_: ICARTCore;
}