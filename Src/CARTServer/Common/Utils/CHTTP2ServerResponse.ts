'use strict';

import { CHTTPStatusCode } from "./CHTTPStatusCode";

export class CHTTP2ServerResponse {
	constructor(_statusCode: CHTTPStatusCode, _cookies: string[], _redirectPath: string, _data: string, _authToken: string, _customHeaders: string[][]) {
		this.statusCode_ = _statusCode;
		this.redirectPath_ = _redirectPath;
		this.data_ = _data;
		this.customHeaders_ = _customHeaders;
		this.cookies_ = _cookies;
		if (_authToken != undefined) this.authToken_ = _authToken;
	}

	public statusCode_: CHTTPStatusCode;
	public redirectPath_: string;
	public customHeaders_: string[][];
	public cookies_: string[];
	public data_: string;
	public authToken_?: string;
}