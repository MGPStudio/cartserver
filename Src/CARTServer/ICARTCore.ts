'use strict';

import { CARTIPCProvider } from "./Common/Utils/CARTIPCProvider";

export interface ICARTCore {
	GetIPCProvider(): CARTIPCProvider;
	Shutdown(_errCode: number): void;
}