'use strict';

import { ICARTCore } from "../../ICARTCore";

export class WebAPIManager {
	constructor(_context: ICARTCore) {
		this.context_ = _context;
	}

	public GetContext(): ICARTCore {
		return this.context_;
	}

	private apiMethodsList_: WebAPIMethod[];
	private context_: ICARTCore;
}