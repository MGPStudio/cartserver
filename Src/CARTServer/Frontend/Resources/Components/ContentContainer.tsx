'use strict';

import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { DynamicPageManager } from '../../DynamicPageManager';

export class ContentContainer extends Component {
	constructor(_context: DynamicPageManager, _props: any) {
		super(_props);
		this.componentsList_ = new Array<Component>();
		this.cssLinks_ = _context.GetRouter().GetStaticResourceUrl() + "static/css/global.css";
	}

	public AddComponent(_components: Component) {
		
	}

	public render() {
		var componentsList: object[] = [];

		this.componentsList_.forEach(component => {
			<div className="row-fluid">
				{component}
			</div>
		});

		return (
			<div id="ContentContainer" className="container-fluid d-flex flex-column">
				{componentsList}
			</div>
		);
	}

	public GetComponentCSS(): string {
		return this.cssLinks_;
	}

	private componentsList_: Component[];
	private cssLinks_: string[];
}