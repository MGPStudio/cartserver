'use strict';

import { SessionType } from "./SessionType";

export class SessionInfo {
	constructor(_sessionType: SessionType, _csrfId: string, _clientUserAgent: string, _clientIp: string) {
		this.sessionType_ = _sessionType;
		this.csrfId_ = _csrfId;
		this.clientUserAgent_ = _clientUserAgent;
		this.clientIp_ = _clientIp;
	}

	public sessionType_: SessionType;
	public csrfId_: string;
	public clientUserAgent_: string;
	public clientIp_: string;
	public accountId_?: number;
	public sessionSecret_?: string;
}