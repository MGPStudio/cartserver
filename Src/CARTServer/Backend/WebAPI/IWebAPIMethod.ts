'use strict';

export interface IWebAPIMethod {
	Call(_params: object[]): string;
}