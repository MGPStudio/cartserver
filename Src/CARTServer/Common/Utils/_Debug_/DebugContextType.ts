'use strict';

export enum DebugContextType {
	CART_FRONTEND = 0,
	CART_BACKEND = 1
}