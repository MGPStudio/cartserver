'use strict';

export enum LogType {
	UNDEFINED = 0,
	INFO = 1,
	WARN = 2,
	ERR = 3
}