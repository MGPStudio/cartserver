'use strict';

import { Router } from "./Utils/Router";
import { ICARTCore } from "../ICARTCore";
import { PageRoot } from "./Pages/PageRoot";

export class DynamicPageManager {
	constructor(_context: ICARTCore, _router: Router, _pageList: string) {
		this.router_ = _router;
		this.context_ = _context;
		
		// TODO: Parse page list into the map, init pages
	}

	public GetRouter() {
		return this.router_;
	}

	private router_: Router;
	private context_: ICARTCore;
	private pages_: Map<string, PageRoot>;
}