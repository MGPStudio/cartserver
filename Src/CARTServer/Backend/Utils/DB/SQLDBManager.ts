/*
 * Copyright (С) MGP Studio 2019, All Rights Reserved.
 * This document is a property of MGP Studio and its distribution is prohibited.
 * INTERNAL USE ONLY
 * 
 * Project: CARTServer (MGPStudio-Web-Internal-CARTServer)
 * Type: Web
 * 
 * File name: SQLDBManager.ts
 * Location: /CARTServer/Utils/DB/
 */

'use strict';

export class SQLDBManager {
	constructor(_dbConfig: DBConfig) {
		// Assert(dbConfig != null);
		
		this.dbConfig_ = _dbConfig;
	}
	
	public dbConfig_: DBConfig;
	public connected_: boolean;
}
