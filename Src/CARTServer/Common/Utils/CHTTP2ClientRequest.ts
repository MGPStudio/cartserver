'use strict';

export class CHTTP2ClientRequest {
	constructor(_host: string, _port: number, _path: string, _clientIp: string, _clientUserAgent: string, _cookies: string[], _authToken: string, _headers: object) {
		this.host_ = _host;
		this.port_ = _port;
		this.path_ = _path;
		this.clientIp_ = _clientIp;
		this.clientUserAgent_ = _clientUserAgent;
		this.cookies_ = _cookies;
		this.authToken_ = _authToken;
		this.headers_ = _headers;
	}

	public host_: string;
	public port_: number;
	public path_: string;
	public clientIp_: string;
	public cookies_: string[];
	public clientUserAgent_: string;
	public authToken_: string;
	public headers_: object;
}