'use strict';

import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { ContentContainer } from '../Resources/Components/ContentContainer';

export class PageRoot extends Component {
	constructor(_props: any, _title: string) {
		super(_props);

		if (_title.length > 0) this.title_ = _title;
		else this.title_ = "";
		this.cssLinks_ = new Array<string>();
		this.faviconLink_ = "";
	}

	public AddStylesheet(_link: string) {
		this.cssLinks_.push(_link);
	}

	public AddFavicon(_link: string) {
		this.faviconLink_ = _link;
	}

	public AddContentContainer(_container: ContentContainer) {
		this.contentContainer_ = _container;
	}

	public render() {
		var linksList: object[] = [];

		this.cssLinks_.forEach(link => {
			linksList.push({
				"rel": "stylesheet", "href": link
			});
		});
		if (this.faviconLink_.length > 0) linksList.push({
			"rel": "icon", "href": this.faviconLink_, "type": "image/x-icon"
		});

		return (
			<div>
				<Helmet
					htmlAttributes = {
						{"lang": "en" }
					}
					title = {this.title_}
					meta = {[
						{ "name": "viewport", "content": "width=device-width, initial-scale=1, shrink-to-fit=no" }
					]}
					link = {linksList}
				/>
				<div>
					{this.contentContainer_}
				</div>
			</div>
		);
	}

	private title_: string;
	private cssLinks_: string[];
	private faviconLink_: string;
	private contentContainer_?: Component;
}