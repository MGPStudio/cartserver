'use strict';

import { CUDPServerConfig } from "./CUDPServerConfig";

export class CUDPServer {
	constructor(_config: CUDPServerConfig) {
		this.config_ = _config;
		this.server_ = null;
	}

	public Init() {
		
	}

	private config_: CUDPServerConfig;
	private server_: any;
}