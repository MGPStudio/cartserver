'use strict';

export enum CIPCProviderType {
	CART_FRONTEND = 0,
	CART_BACKEND = 1
}