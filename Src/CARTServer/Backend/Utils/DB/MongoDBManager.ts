/*
 * Copyright (С) MGP Studio 2019, All Rights Reserved.
 * This document is a property of MGP Studio and its distribution is prohibited.
 * INTERNAL USE ONLY
 * 
 * Project: CARTServer (MGPStudio-Web-Internal-CARTServer)
 * Type: Web
 * 
 * File name: MongoDBManager.ts
 * Location: /Src/CARTServer/Backend/Utils/DB/
 */
'use strict';


const mongoClient = require('mongodb').MongoClient;


import { DBConfig } from "./DBConfig";
import { ICARTDBManagerCallbackProvider } from "./ICARTDBManagerCallbackProvider";
import { ICARTDBManager } from "./ICARTDBManager";


export class MongoDBManager implements ICARTDBManager {
	
	constructor(_dbConfig: DBConfig) {
		this.dbConfig_ = new DBConfig();
		this.connected_ = false;
		this.client_ = null;
		this.dbObject_ = null;
	}

	Init(_dbConfig: DBConfig): void {
		// Assert(_dbConfig.dbType_ == MONGODB);

		this.dbConfig_ = _dbConfig;
	}

	public async Connect(_callbackProvider: ICARTDBManagerCallbackProvider) {
		var result = mongoClient.connect(this.dbConfig_.dbHost_)
		await result.then((_client: any) => {
			console.log("Connected to DB.\n");
			this.connected_ = true;
			this.client_ = _client;
			this.dbObject_ = this.client_.db(this.dbConfig_.dbName_);
			_callbackProvider.OnConnectCallback(this.connected_);
		})
		.catch((_err: any) => {
			console.log(_err);
			_callbackProvider.OnConnectCallback(this.connected_);
		});
	}

	public Disconnect(): void {
		if (!this.connected_) return;
		if (this.client_ == null) {
			// Signal exception
			return;
		}

		this.client_.close();
	}

	public GetCollectionDocuments(_collectionName: string, _aggregator: Array<object>, _callbackProvider: ICARTDBManagerCallbackProvider) {
		this.dbObject_.collection(_collectionName).aggregate( _aggregator).toArray()
		.then((_docs: any) => {
			_callbackProvider.OnGetSingleStorageDataCallback(_docs);
		})
		.catch((_err: any) => {
			console.log(_err);
			_callbackProvider.OnGetSingleStorageDataCallback(null);
		});
	}

	public GetConnectionState(): boolean {
		return this.connected_;
	}
	
	private dbConfig_: DBConfig;
	private connected_: boolean;
	private dbObject_: any;
	private client_: any;
}
