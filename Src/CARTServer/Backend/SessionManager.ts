'use strict';

import { ICARTCore } from "../ICARTCore";
import { SessionInfo } from "./SessionInfo";
import { DebugUtils } from "../Common/Utils/_Debug_/DebugUtils";
import { CARTFrontendCore } from "../Frontend/CARTFrontendCore";

const SESSIONS_MAX_COUNT = 5000;

export class SessionManager {
	constructor(_context: ICARTCore, _maxSessions: number) {
		this.context_ = _context;

		(this.context_ as CARTFrontendCore)._debugUtils_.Assert(_maxSessions > 0, this);
		(this.context_ as CARTFrontendCore)._debugUtils_.Assert(_maxSessions <= SESSIONS_MAX_COUNT, this);
		this.maxSessions_ = _maxSessions;

		this.sessionInfoArr_ = new Array(this.maxSessions_);
		this.currentSessions_ = 0;
	}
	
	private context_: ICARTCore;
	private sessionInfoArr_: SessionInfo[];
	private maxSessions_: number;
	private currentSessions_: number;
}