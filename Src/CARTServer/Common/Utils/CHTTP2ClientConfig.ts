'use strict';

export class CHTTP2ClientConfig {
	constructor(_sslCertPath: string,  _sslCACertPath: string) {
		// Assert(_sslCertPath.length != 0);
		// Assert(_sslCAPath.length != 0);

		this.sslCACertPath_ = _sslCACertPath;
		this.sslCertPath_ = _sslCertPath;
	}

	public sslCertPath_: any;
	public sslKeyPath_: any;
	public sslCACertPath_: any;
}