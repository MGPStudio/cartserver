'use strict';

export class CHTTP2ServerConfig {
	constructor(_port: number, _allowedOrigins: Array<string>, _sslCertPath: string, _sslKeyPath: string, _sslCACertPath: string) {
		// Assert(_port > 0);
		// Assert(_allowedOrigins.length > 0);
		// Assert(_sslCertPath.length != 0);
		// Assert(_sslKeyPath.length != 0);
		// Assert(_sslCACertPath.length != 0);

		this.port_ = _port;
		this.allowedOrigins_ = _allowedOrigins;
		this.sslCACertPath_ = _sslCACertPath;
		this.sslCertPath_ = _sslCertPath;
		this.sslKeyPath_ = _sslKeyPath;
	}

	public port_: number;
	public allowedOrigins_: Array<string>;
	public sslCertPath_: any;
	public sslKeyPath_: any;
	public sslCACertPath_: any;
}