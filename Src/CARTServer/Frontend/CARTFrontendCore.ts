'use strict';

import { ICARTCore } from "../ICARTCore";
import { CARTIPCProvider } from "../Common/Utils/CARTIPCProvider";
import { DynamicPageManager } from "./DynamicPageManager";
import { DebugUtils } from "../Common/Utils/_Debug_/DebugUtils";
import { DebugContextType } from "../Common/Utils/_Debug_/DebugContextType";
import { CHTTP2Server } from "../Common/Utils/CHTTP2Server";
import { CHTTP2ServerConfig } from "../Common/Utils/CHTTP2ServerConfig";
import { LogType } from "../Common/Utils/_Debug_/LogType";
import { CIPCProviderType } from "../Common/Utils/CIPCProviderType";
import { Router } from "./Utils/Router";

const path = require('path');

export class CARTFrontendCore implements ICARTCore {
	constructor() {
		this._debugUtils_ = new DebugUtils(DebugContextType.CART_FRONTEND, this);

		if (process.env.CARTSERVER_ROOT_DIR == undefined)  this.rootDir_ = "test";
		else this.rootDir_ = process.env.CARTSERVER_ROOT_DIR;

		var ipcSecret: string = (process.env.CARTSERVER_FRONTEND_IPC_SECRET || "");
		if (ipcSecret.length === 0) this._debugUtils_.Log(LogType.ERR, true, "CARTFrontendCore Init Error: IPC secret not defined.");
		this.ipcProvider_ = new CARTIPCProvider(CIPCProviderType.CART_BACKEND, "", 0, ipcSecret);
		this.router_ = new Router(this, null, null);

		// TODO: Remove before MMSG integration
		var serverConfig = new CHTTP2ServerConfig(8081, 
		[
			"https://localhost"
		], 
		path.normalize(this.rootDir_ + "config/certs/localhost/mgpwebcert-dev-localhost.signed.cer"), 
		path.normalize(this.rootDir_ + "config/certs/localhost/mgpwebcert-dev-localhost.key"), 
		path.normalize(this.rootDir_ + "config/certs/ca/MGPRootCert.pem"));
		this.http2Server_ = new CHTTP2Server(this, serverConfig);
		this.http2Server_.Init();
	}

	GetIPCProvider(): CARTIPCProvider {
		throw new Error("Method not implemented.");
	}

	Shutdown(_errCode: number): void {
		// TODO: Remove before MMSG integration
		this.http2Server_.Shutdown();


		process.exit(_errCode);
	}

	public GetRouter(): Router {
		return this.router_;
	}
	
	private ipcProvider_: CARTIPCProvider;
	public _debugUtils_: DebugUtils;
	private router_: Router;


	// TODO: Remove before MMSG integration
	private rootDir_: string;
	private http2Server_: CHTTP2Server;
}