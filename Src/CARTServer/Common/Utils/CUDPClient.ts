'use strict';

import { CUDPClientConfig } from "./CUDPClientConfig";

export class CUDPClient {
	constructor(_config: CUDPClientConfig) {
		this.config_ = _config;
		this.client_ = null;
	}

	public Init() {
		
	}

	private config_: CUDPClientConfig;
	private client_: any;
}