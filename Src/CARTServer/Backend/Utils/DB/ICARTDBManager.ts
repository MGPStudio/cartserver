'use strict';

import { DBConfig } from "./DBConfig";
import { ICARTDBManagerCallbackProvider } from "./ICARTDBManagerCallbackProvider";

export interface ICARTDBManager {
	Init(_dbConfig: DBConfig): void;
	Connect(_callbackProvider: ICARTDBManagerCallbackProvider): void;
	GetConnectionState(): boolean;
}