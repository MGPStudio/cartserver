**CARTServer - Client App Reporting Tool Server**  
  
Includes:  
  
- Database: MongoDB  
- Backend: Node.JS  
- Frontend: React + Bootstrap  
  
This repository contains a demo of CARTServer for V.N. Karazin Kharkiv National University. Only for educational use.  
  
(c) MGP Studio, 2019. All Rights Reserved.