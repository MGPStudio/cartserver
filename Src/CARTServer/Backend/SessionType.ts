'use strict';

export enum SessionType {
	GUEST = 0,
	ACCOUNT_MGP = 1
}